import skimage
from skimage.io  import imread, imsave, imshow, show, imread_collection, imshow_collection
from skimage import color
import matplotlib.pylab as plt
#print(skimage.__version__)


def showIma( path , h=6, w=6, ax= 'off'  ):
	im= imread(path)  # read the image, provide the correct path
	plt.figure(figsize=(h,w))
	plt.imshow(im) # display the image
	plt.axis(ax)
	plt.show()


def showHSVIma(path, h=6, w=6, ax='off'  ):
	img= imread(path)  # read the image, provide the correct path
	hsv_img = color.rgb2hsv(img)
	hue_img = hsv_img[:, :, 0]
	sat_img = hsv_img[:, :, 1]
	val_img = hsv_img[:, :, 2]
	fig, (ax0, ax1, ax2, ax3, ax4) = plt.subplots(ncols=5, figsize=(h, w))
	ax0.imshow(img)
	ax0.set_title("RGB image")
	ax0.axis('off')
	ax1.imshow(hue_img, cmap='hsv')
	ax1.set_title("HSV image")
	ax1.axis('off')
	ax2.imshow(hue_img)
	ax2.set_title("hue channel")
	ax2.axis('off')
	ax3.imshow(sat_img)
	ax3.set_title("saturation channel")
	ax3.axis('off')
	ax4.imshow(val_img)
	ax4.set_title("value channel")
	ax4.axis('off')
	fig.tight_layout()
	


def showHSVIma2(path, h=6, w=6, ax='off'  ):
	img= imread(path)  # read the image, provide the correct path
	im_hsv = color.rgb2hsv(img)
	plt.gray()
	plt.figure(figsize=(h,w))
	plt.subplot(221), plt.imshow(im_hsv[...,0]), plt.title('h', size=20),
	plt.axis('off')
	plt.subplot(222), plt.imshow(im_hsv[...,1]), plt.title('s', size=20),
	plt.axis('off')
	plt.subplot(223), plt.imshow(im_hsv[...,2]), plt.title('v', size=20),
	plt.axis('off')
	plt.subplot(224), plt.axis('off')
	plt.show()

def showGrScIma( path, h=6, w=6, ax= 'off'):
	im= imread(path, as_gray=True) # read the image, provide the correct path
	plt.figure(figsize=(h,w))
	plt.imshow(im,cmap='gray') # display the image
	plt.axis(ax)
	plt.show()
