'''
 * Python program to create a color histogram.
 *
 * Usage: python ColorHistogram.py <filename>
'''
import cv2
import sys
from matplotlib import pyplot as plt


def showColorImg(path):
	img = cv2.imread(path)
	img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
	plt.axis("off")
	plt.imshow(img)
	plt.show()

def showGrayImg(path):
	img = cv2.imread(path)
	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	plt.axis("off")
	plt.imshow(img)
	plt.gray()
	plt.show()

def getHisRGB(path):
	# read original image, in full color, based on command
	# line argument
	img = cv2.imread(path)
	# display the image 
	#cv2.namedWindow("Original Image", cv2.WINDOW_NORMAL)
	#cv2.imshow("Original Image", img)
	#cv2.waitKey(0)
	# split into channels
	channels = cv2.split(img)
	# list to select colors of each channel line
	colors = ("r", "g", "b") 
	# create the histogram plot, with three lines, one for
	# each color
	plt.xlim([0, 256])
	for(channel, c) in zip(channels, colors):
	    histogram = cv2.calcHist([channel], [0], None, [256], [0, 256])
	    plt.plot(histogram, color = c)
	plt.xlabel("Color value")
	plt.ylabel("Pixels")
	plt.show()

def getHisGrayScale(path):
	img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
	# display the image
	#cv2.namedWindow("Grayscale Image", cv2.WINDOW_NORMAL)
	#cv2.imshow("Grayscale Image", img)
	#cv2.waitKey(0)
	# create the histogram
	histogram = cv2.calcHist([img], [0], None, [256], [0, 256])
	# configure and draw the histogram figure
	plt.figure()
	plt.title("Grayscale Histogram")
	plt.xlabel("grayscale value")
	plt.ylabel("pixels")
	plt.xlim([0, 256])
	plt.plot(histogram, color= "gray")
	plt.show()


def main():
    if(len(sys.argv) < 1):
        print("Use: getColorHistogram.py <<filename>>")
        quit()
    imaName=  sys.argv[1]
    extractMeta(path)
 

if __name__ == "__main__":
    main()
