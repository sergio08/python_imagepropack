import matplotlib.image as mpimg
import matplotlib.pylab as plt
import numpy as np

def plot_circular_mask(path, center=None, radius=None, fig_size= 3):
	"""
	https://stackoverflow.com/questions/44865023/circular-masking-an-image-in-python-using-numpy-arrays
	"""
	im= mpimg.imread(path)
	im.setflags(write=1)
	h, w = im.shape[:2]
	if center is None:
	 	center = [int(w/2), int(h/2)] # use the middle of the image
	if radius is None: # use the smallest distance between the center and image walls
	    radius = min(center[0], center[1], w-center[0], h-center[1])
	Y, X = np.ogrid[:h, :w] #The function np.ogrid() gives two vectors, each containing the pixel locations (or indices)
	dist_from_center = np.sqrt((X - center[0])**2 + (Y-center[1])**2)
	mask = dist_from_center <= radius
	im[~mask] = 0
	plt.figure(figsize=(fig_size,fig_size))
	plt.imshow(im), plt.axis('off'), plt.show()
