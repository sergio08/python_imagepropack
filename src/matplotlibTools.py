import matplotlib.image as mpimg
import matplotlib.pylab as plt




def showIma( path , h=6, w=6, ax= 'off'  ):
	im= mpimg.imread(path)  # read the image, provide the correct path
	plt.figure(figsize=(h,w))
	plt.imshow(im) # display the image
	plt.axis(ax)
	plt.show()


def darkIma( path , h=6, w=6, thresold=0.5, ax= 'off' ):
	im= mpimg.imread(path)  # read the image, provide the correct path
	im.setflags(write=1)
	im[im < thresold] = 0 
	plt.figure(figsize=(h,w))
	plt.imshow(im) # display the image
	plt.axis(ax)
	plt.show()